// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"net"

	socketmap "codeberg.org/bergblau/postfix-socketmap-table"
)

var testmap = map[string]string{
	"foo@example.com": "bar@example.com",
	"baz@example.com": "cux@example.com",
}
	
func lookup(key string) (*socketmap.SocketMapResult, error) {
	v, exists := testmap[key]
	if exists {
		return socketmap.Ok(v), nil
	}
	return socketmap.NotFound(), nil
}

func main() {
	server := socketmap.NewSocketMapServer()
	server.RegisterMap("aliases", lookup)

	listener, err := net.Listen("tcp", "127.0.0.1:17171")
	if err != nil {
		panic(err)
	}
	server.Serve(listener)
}
