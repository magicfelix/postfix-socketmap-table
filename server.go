// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package socketmap implements Postfix' socketmap_table lookup protocol.
//
// # Background
//
// The [Postfix] mail server uses lookup tables in various places, when it
// needs to translate some value into another. The most common use case
// is alias expansion for mail addresses.
//
// While most tasks can be achieved using Postfix' built-in tables and flat
// files, sometimes, a more dynamic approach is needed. Therefore, Postfix
// can query external services using the [socketmap_table] protocol.
//
// The protocol originates from sendmail, which should also be able to
// use services implemented using this library.
//
// # Implementing a lookup server
//
// To implement a socketmap_table lookup server, only two things are needed:
//
//  1. A lookup function with the signature of [LookupFn] which implements
//     the lookup itself and returns a response
//  2. A network [net.Listener] to listen for Postfix' connections on
//
// The source code repository has [examples].
//
// A single server can handle many different named maps, by registering
// different lookup functions with the [SocketMapServer.RegisterMap] function.
//
// [Postfix]: https://www.postfix.org/
// [socketmap_table]: https://www.postfix.org/socketmap_table.5.html
// [examples]: https://codeberg.org/Bergblau/postfix-socketmap-table/src/branch/main/examples/
package socketmap

import (
	"net"

	"github.com/seandlg/netstring"
)

// A server handling socketmap_table connections on one network listener
type SocketMapServer struct {
	listeners []net.Listener
	maps socketMapMap
}

// Lookup function called by the [SocketMapServer] for each request to the map it is registered for
type LookupFn func(key string) (*SocketMapResult, error)

type socketMapMap map[string]LookupFn

// Create a new [SocketMapServer] with an empty mapping map
func NewSocketMapServer() *SocketMapServer {
	return &SocketMapServer{
		maps: make(socketMapMap),
	}
}

// Register a lookup function as a named map
// The map name is sent by Postfix, according to its configuration.
func (s *SocketMapServer) RegisterMap(name string, fn LookupFn) {
	s.maps[name] = fn
}

// Register a lookup function as a named map
// The map name is sent by Postfix, according to its configuration.
func (s *SocketMapServer) Serve(listener net.Listener) error {
	defer listener.Close()
	s.listeners = append(s.listeners, listener)

	for {
		conn, err := listener.Accept()
		if err != nil {
			return err
		}
		go s.handle(conn)
	}
}

func (s *SocketMapServer) handle(conn net.Conn) error {
	defer conn.Close()

	for {
		ns := netstring.ForReading()

		for {
			err := ns.ReadFrom(conn)
			if err == nil {
				// netstring is complete
				break
			}
			if err == netstring.Incomplete {
				// netstring is not fully read, wait for more data
				continue
			}
			// Data was garbled or the connection broke, bail out
			return err
		}

		// Build request from netstring
		r := &SocketMapRequest{}
		if err := r.Decode(ns); err != nil {
			return err
		}

		// Determine lookup function
		fn, exists := s.maps[r.Name]
		var res *SocketMapResult
		var err error
		if exists {
			res, err = fn(r.Key)
			if err != nil {
				// This error happened on the backend, so we do not need to break the connection
				res = TempFail(err.Error())
			}
		} else {
			// The map does not exist, this is a temporary configuration failure probably
			res = TempFail("The lookup map does not exist")
		}

		// Finally, send along netstring data of result
		b, err := res.Encode().Marshal()
		if err != nil {
			// Failing to encode a message, break this connection
			return err
		}
		_, err = conn.Write(b)
		if err != nil {
			// Writing to the network failed, break this connection
			return err
		}
	}
}
