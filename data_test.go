// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package socketmap_test

import (
	"bytes"
	"testing"

	"github.com/seandlg/netstring"

	socketmap "codeberg.org/bergblau/postfix-socketmap-table"
)

func TestRequestDecode(t *testing.T) {
	b := []byte("23:testmap foo@example.com,")
	ns := netstring.ForReading()
	ns.ReadFrom(bytes.NewReader(b))

	r := &socketmap.SocketMapRequest{}
	if err := r.Decode(ns); err != nil {
		t.Errorf("Failed to decode: %s", err)
	}
	if r.Name != "testmap" {
		t.Errorf("Got unexpected map name: %s", r.Name)
	}
	if r.Key != "foo@example.com" {
		t.Errorf("Got unexpected lookup key: %s", r.Key)
	}
}

func TestResultEncode(t *testing.T) {
	r := &socketmap.SocketMapResult{
		Status: "OK",
		Data: "bar@example.com",
	}

	be := []byte("18:OK bar@example.com,")
	bg, err := r.Encode().Marshal()
	if err != nil {
		t.Errorf("Failed to encode: %s", err)
	}
	if string(bg) != string(be) {
		t.Errorf("Encoding yielded invalid netstring data")
	}
}
