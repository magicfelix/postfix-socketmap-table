# Go implementation for Postfix' socketmap_table lookup protocol

[![Go Reference](https://pkg.go.dev/badge/codeberg.org/bergblau/postfix-socketmap-table.svg)](https://pkg.go.dev/codeberg.org/bergblau/postfix-socketmap-table)

Package socketmap implements Postfix' socketmap_table lookup protocol.

# Background

The [Postfix] mail server uses lookup tables in various places, when it
needs to translate some value into another. The most common use case
is alias expansion for mail addresses.

While most tasks can be achieved using Postfix' built-in tables and flat
files, sometimes, a more dynamic approach is needed. Therefore, Postfix
can query external services using the [socketmap_table] protocol.

The protocol originates from sendmail, which should also be able to
use services implemented using this library.

# Implementing a lookup server

To implement a socketmap_table lookup server, only two things are needed:

 1. A lookup function which implements the lookup itself and returns a response
 2. A network listener to listen for Postfix' connections on

The source code repository has [examples].

A single server can handle many different named maps, by registering
different lookup functions.

# Origin and maintenance

This package was written for the [Bergblau] component [WebMilterd], which,
among otehr functionality, resolves aliases by querying a REST API.

It is maintained by [velocitux], which also offers commercial support
for this lookup mechanism and for Postfix in general.

Special thanks to [Claus Assmann] for his [proposal] to use the socketmap_table.

[Postfix]: https://www.postfix.org/
[socketmap_table]: https://www.postfix.org/socketmap_table.5.html
[examples]: https://codeberg.org/Bergblau/postfix-socketmap-table/src/branch/main/examples/
[Bergblau]: https://bergblau.io/
[WebMilterd]: https://docs.bergblau.io/components/webmilterd/
[velocitux]: https://www.velocitux.com/
[Claus Assmann]: https://www.oreilly.com/pub/au/1930
[proposal]: https://marc.info/?l=postfix-users&m=166021362607009&w=2
