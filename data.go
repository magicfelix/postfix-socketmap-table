// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package socketmap

import (
	"fmt"
	"strings"

	"github.com/seandlg/netstring"
)

// A request as received from Postfix
type SocketMapRequest struct {
	Name string
	Key string
}

// A result sent to Postfix
type SocketMapResult struct {
	Status string
	Data string
}

// Encode the result into a netstring object for further serialization
func (r *SocketMapResult) Encode() *netstring.Netstring {
	return netstring.From([]byte(r.Status + " " + r.Data))
}

// Decode the request from a netstring object after deserialization
func (r *SocketMapRequest) Decode(ns *netstring.Netstring) error {
	// Get the original content of the message
	b, err := ns.Bytes()
	if err != nil {
		return err
	}

	// The message needs to contain a map name and the lookup key, separated by a space
	parts := strings.SplitN(string(b), " ", 2)
	if len(parts) != 2 {
		return fmt.Errorf("Malformed request")
	}
	r.Name = parts[0]
	r.Key = parts[1]

	return nil
}

// Produce an OK response
func Ok(data string) *SocketMapResult {
	return &SocketMapResult{
		Status: "OK",
		Data: data,
	}
}

// Produce a NOTFOUND response
func NotFound() *SocketMapResult {
	return &SocketMapResult{
		Status: "NOTFOUND",
		Data: "",
	}
}

// Produce a TEMP response
func TempFail(reason string) *SocketMapResult {
	return &SocketMapResult{
		Status: "TEMP",
		Data: reason,
	}
}

// Produce a TIMEOUT response
func Timeout(reason string) *SocketMapResult {
	return &SocketMapResult{
		Status: "TIMEOUT",
		Data: reason,
	}
}

// Produce a PERM response
func PermFail(reason string) *SocketMapResult {
	return &SocketMapResult{
		Status: "PERM",
		Data: reason,
	}
}
