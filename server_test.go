// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package socketmap_test

import (
	"net"
	"golang.org/x/net/nettest"
	"testing"

	"github.com/seandlg/netstring"
	
	socketmap "codeberg.org/bergblau/postfix-socketmap-table"
)

var testmapCom = map[string]string{
	"foo@example.com": "bar@example.com",
	"baz@example.com": "cux@example.com",
}
var testmapOrg = map[string]string{
	"foo@example.org": "bar@example.org",
	"baz@example.org": "cux@example.org",
}
	
func fnCom(key string) (*socketmap.SocketMapResult, error) {
	v, exists := testmapCom[key]
	if exists {
		return socketmap.Ok(v), nil
	}
	return socketmap.NotFound(), nil
}
func fnOrg(key string) (*socketmap.SocketMapResult, error) {
	v, exists := testmapOrg[key]
	if exists {
		return socketmap.Ok(v), nil
	}
	return socketmap.NotFound(), nil
}

func runTestServer() (net.Conn, error) {
	server := socketmap.NewSocketMapServer()
	server.RegisterMap("com", fnCom)
	server.RegisterMap("org", fnOrg)

	listener, err := nettest.NewLocalListener("tcp")
	if err != nil {
		return nil, err
	}
	go server.Serve(listener)

	addr := listener.Addr()
	conn, err := net.Dial(addr.Network(), addr.String())
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func sendRequest(conn net.Conn, bin []byte) ([]byte, error) {
	conn.Write(bin)
	ns := netstring.ForReading()
	err := ns.ReadFrom(conn)
	if err != nil {
		return nil, err
	}
	return ns.Marshal()
}

func TestServerOneRequestOk(t *testing.T) {
	conn, err := runTestServer()
	if err != nil {
		t.Errorf("Failed to run test server: %s", err)
		t.FailNow()
	}

	bin := []byte("19:com foo@example.com,")
	be := []byte("18:OK bar@example.com,")
	bg, err := sendRequest(conn, bin)

	if err != nil {
		t.Errorf("Error sending request: %s", err)
		t.FailNow()
	}
	if string(bg) != string(be) {
		t.Errorf("Got unexpected response: %s", string(bg))
	}
}

func TestServerOneRequestNotFound(t *testing.T) {
	conn, err := runTestServer()
	if err != nil {
		t.Errorf("Failed to run test server: %s", err)
		t.FailNow()
	}

	bin := []byte("19:com fax@example.com,")
	be := []byte("9:NOTFOUND ,")
	bg, err := sendRequest(conn, bin)

	if err != nil {
		t.Errorf("Error sending request: %s", err)
		t.FailNow()
	}
	if string(bg) != string(be) {
		t.Errorf("Got unexpected response: %s", string(bg))
	}
}

func TestServerTwoRequestsOk(t *testing.T) {
	conn, err := runTestServer()
	if err != nil {
		t.Errorf("Failed to run test server: %s", err)
		t.FailNow()
	}

	bin := []byte("19:com foo@example.com,")
	be := []byte("18:OK bar@example.com,")
	bg, err := sendRequest(conn, bin)
	if err != nil {
		t.Errorf("Error sending request: %s", err)
		t.FailNow()
	}
	if string(bg) != string(be) {
		t.Errorf("Got unexpected response: %s", string(bg))
	}

	bin = []byte("19:com baz@example.com,")
	be = []byte("18:OK cux@example.com,")
	bg, err = sendRequest(conn, bin)
	if err != nil {
		t.Errorf("Error sending request: %s", err)
		t.FailNow()
	}
	if string(bg) != string(be) {
		t.Errorf("Got unexpected response: %s", string(bg))
	}
}

func TestServerTwoRequestsOkMixedMaps(t *testing.T) {
	conn, err := runTestServer()
	if err != nil {
		t.Errorf("Failed to run test server: %s", err)
		t.FailNow()
	}

	bin := []byte("19:com foo@example.com,")
	be := []byte("18:OK bar@example.com,")
	bg, err := sendRequest(conn, bin)
	if err != nil {
		t.Errorf("Error sending request: %s", err)
		t.FailNow()
	}
	if string(bg) != string(be) {
		t.Errorf("Got unexpected response: %s", string(bg))
	}

	bin = []byte("19:org baz@example.org,")
	be = []byte("18:OK cux@example.org,")
	bg, err = sendRequest(conn, bin)
	if err != nil {
		t.Errorf("Error sending request: %s", err)
		t.FailNow()
	}
	if string(bg) != string(be) {
		t.Errorf("Got unexpected response: %s", string(bg))
	}
}

func TestServerOneRequestUnknownMap(t *testing.T) {
	conn, err := runTestServer()
	if err != nil {
		t.Errorf("Failed to run test server: %s", err)
		t.FailNow()
	}

	bin := []byte("19:net foo@example.com,")
	be := []byte("34:TEMP The lookup map does not exist,")
	bg, err := sendRequest(conn, bin)
	if err != nil {
		t.Errorf("Error sending request: %s", err)
		t.FailNow()
	}
	if string(bg) != string(be) {
		t.Errorf("Got unexpected response: %s", string(bg))
	}
}
