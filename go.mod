module codeberg.org/bergblau/postfix-socketmap-table

go 1.18

require (
	github.com/seandlg/netstring v1.0.0
	golang.org/x/net v0.0.0-20220809184613-07c6da5e1ced
)
